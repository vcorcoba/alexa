
const AnimalesList = [
    {Nombre: 'Perro', Sonido: '<audio src="soundbank://soundlibrary/animals/amzn_sfx_dog_med_bark_1x_02"/>', Foto:'<image source="https://images.ecestaticos.com/h12WExjneaKjOxgyy645vKGtOEM=/0x115:2265x1390/1600x900/filters:fill(white):format(JPG)/f.elconfidencial.com/original/8ec/08c/85c/8ec08c85c866ccb70c4f1c36492d890f.jpg"/>'},
    {Nombre: 'Gato', Sonido: '<audio src="soundbank://soundlibrary/animals/amzn_sfx_cat_meow_1x_01"/>', Foto: '<image source="https://images.ecestaticos.com/FVdcvD11qPRi-JWDH3USTiXDmeQ=/0x0:2120x1414/1200x900/filters:fill(white):format(jpg)/f.elconfidencial.com%2Foriginal%2F47b%2F328%2F963%2F47b3289639713b8e80c8d682d219fba7.jpg"/>'},
    {Nombre: 'Caballo', Sonido: '<audio src="soundbank://soundlibrary/animals/amzn_sfx_horse_huff_whinny_01"/>', Foto: '<image source="https://www.equusline.es/wp-content/uploads/2016/08/67velocidad-caballo.jpg"/>'},
    {Nombre: 'Pato', Sonido: 'cuacua', Foto: '<image source="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a1/Mallard2.jpg/1200px-Mallard2.jpg"/>'},
    {Nombre: 'Pajaro', Sonido: '<audio src="soundbank://soundlibrary/animals/amzn_sfx_bird_forest_01"/>', Foto: '<image source="https://okdiario.com/img/2020/02/24/-como-saber-si-un-pajaro-es-macho-o-hembra_-655x368.jpg"/>'},
    {Nombre: 'Oveja', Sonido: '<audio src="soundbank://soundlibrary/animals/amzn_sfx_sheep_bleat_03"/>', Foto: '<image source="https://content.quizzclub.com/trivia/2020-01/donde-nacio-la-oveja-dolly-el-primer-mamifero-clonado-a-partir-de-una-celula-adulta.jpg"/>'},
    {Nombre: 'Lobo', Sonido: 'auuuu', Foto: '<image source="https://imagenes.elpais.com/resizer/LzbPq_CSy9ViLuCnmzQ92p6unRc=/414x0/arc-anglerfish-eu-central-1-prod-prisa.s3.amazonaws.com/public/SDOWEZZ3WW2A6HN2QIENLFQGU4.jpg"/>'},
    {Nombre: 'Vaca', Sonido: 'muu', Foto: '<image source="https://storage.contextoganadero.com/s3fs-public/ganaderia/field_image/2017-03/vaca_bonita.jpg"/>'},
    {Nombre: 'León', Sonido: '<audio src="soundbank://soundlibrary/animals/amzn_sfx_lion_roar_03"/>', Foto: '<image source="https://revistaronda.net/wp-content/uploads/2020/09/el-rey-leon.jpg"/>'},
    {Nombre: 'Gallo', Sonido: 'kikiriki', Foto: '<image source="https://definicion.de/wp-content/uploads/2021/04/gallo.jpg"/>'},
    {Nombre: 'Mono', Sonido: '', Foto: '<image source="https://okdiario.com/img/2019/08/24/-es-posible-tener-un-mono-en-cautiverio_.jpg"/>'},
];