function aleatorio(a) {
    let currentIndex = a.length, temporaryValue, randomIndex;
    
    while(currentIndex !== 0) {
        randomIndex = Math.floor(Math.random()*currentIndex);
        currentIndex--;
        temporaryValue = a[currentIndex];
        a[currentIndex] = a[randomIndex];
        a[randomIndex] = temporaryValue;
    }
    return a;
}

function respuestaIncorrecta() {
    return 'Inténtalo otra vez. ¿Qué animal es este?';
}

function respuestaCorrecta() {
    return '<audio src="soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_positive_response_01"/>' + '<say-as interpret-as="interjection">Muy bien</say-as>. Siguiente pregunta';
}

function preguntaAnimal() {
    return '¿Qué animal es este?';
}

function preguntaAnimalSonido() {
    return '¿Qué sonido hace este animal?';
}