module.exports = {
    es:{
        translation: {
            WELCOME_MSG: 'Bienvenido a divierteTEA. Antes de empezar te haré dos preguntas.',
            NAME_MSG: '¿Cómo te llamas?',
            PANTALLA_MSG: '%s, ¿Cómo quieres jugar, con pantalla o sin pantalla?',
            PREG_JUEGO_MSG: '%s, ¿a qué quieres jugar?',
            ANIMALES_PANTALLA_NIVELES: 'Escoge un nivel. Nivel 1 fotos y sonidos. Nivel 2 fotos. Nivel 3 sonidos. ¿Cuál quieres, 1, 2 o 3?',
            ANIMALES_SONIDO_NIVELES: 'Escoge un nivel. Nivel 1 nombre. Nivel 2 sonidos ¿Cuál quieres 1 o 2?',
            ANIMALES_INTRO_PANTALLA: '%s, te voy a enseñar unas fotos y tienes que decir que animal es.',
            ANIMALES_INTRO2_PANTALLA: '%s, tienes que decirme que sonido hacen estos animales.',
            ANIMALES_INTRO_SONIDO: '%s, te voy a poner sonidos de animales y tienes que decir que animal es.',
            ANIMALES_INTRO2_SONIDO: '%s, te voy a decir un animal y tienes que decirme que sonido hace.',
            ANIMALES_PREGUNTA: '¿Qué animal es este?',
            HELP_MSG: 'Dime a qué quieres jugar',
            GOODBYE_MSG: '¡Nos vemos otro día!',
            REFLECTOR_MSG: 'Vamos a jugar. Repite, por favor',
            FALLBACK_MSG: 'Lo siento, dime un juego',
            ERROR_MSG: 'Lo siento, repítelo otra vez'
        }
    }
}